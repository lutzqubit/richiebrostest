﻿using Prism.Modularity;
using Prism.Unity;
using RichieBros.Views;


namespace RichieBros
{
    public partial class App : PrismApplication
    {

       

        protected override void OnInitialized()
        {
            InitializeComponent();

            NavigationService.Navigate("MainMasterDetailPage/MainNavigationPage/AuctionsPage", animated:false);
        }

        protected override void RegisterTypes()
        {
            Container.RegisterTypeForNavigation<MainPage>();
            Container.RegisterTypeForNavigation<MainNavigationPage>();
            Container.RegisterTypeForNavigation<MainMasterDetailPage>();
            Container.RegisterTypeForNavigation<AuctionsPage>();
            Container.RegisterTypeForNavigation<EquipmentPage>();
            Container.RegisterTypeForNavigation<AppraisalsPage>();
            Container.RegisterTypeForNavigation<AboutPage>();
        }

    
       
    }
}
