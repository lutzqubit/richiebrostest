﻿using RichieBros.Models;
using RichieBros.Resources;
using RichieBros.ViewModels;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Xamarin.Forms;

namespace RichieBros.Views
{
    public partial class MainMasterDetailPage : MasterDetailPage 
    {
       

        public MainMasterDetailPage()
        {
            InitializeComponent();

         
            //  listView.ItemsSource = MasterPageItems;
            listView.ItemTapped += (object sender, ItemTappedEventArgs e) =>
              {
                  if (e.Item == null) return;

                  ((ListView)sender).SelectedItem = null;
                 
                  /*
                  foreach(MasterPageItem m in MasterPageItems)
                  {
                      if (((MasterPageItem)e.Item).Title == m.Title)
                      {
                          m.Color = Color.Red;
                      }
                      else
                          m.Color = Color.Black;
                  }

                  */
              };

        }

        public bool IsPresentAfterNavigation
        {
            get { return Device.Idiom != TargetIdiom.Phone; }
        }

    }
}