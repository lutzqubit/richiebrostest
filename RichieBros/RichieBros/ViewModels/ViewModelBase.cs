﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// A blank template that inherits from the prosm Bindable base container and INavigationAware interface
/// </summary>
namespace RichieBros.ViewModels
{
    public class ViewModelBase : BindableBase , INavigationAware
    {
        public ViewModelBase()
        {

        }

        public virtual void OnNavigatedFrom(NavigationParameters parameters)
        {
          
        }

        public virtual void OnNavigatedTo(NavigationParameters parameters)
        {
            
        }
    }
}
