﻿using Xamarin.Forms;

namespace Core.Views
{
    public partial class BaseContentPage : ContentPage
    {
        public BaseContentPage()
        {
            InitializeComponent();
        }
    }
}
